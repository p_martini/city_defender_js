# City defender

Defend the city against the enemy's bombs!

## Play without cloning the repository

➥ [Link to the game](http://usefulweb.services/city_defender_js/game.html)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)